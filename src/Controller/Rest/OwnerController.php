<?php

namespace App\Controller\Rest;

use App\Dto\Request\Owner\OwnerStoreRequest;
use App\Dto\Request\Owner\OwnerUpdateRequest;
use App\Service\OwnerService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class OwnerController extends AbstractFOSRestController
{
    /** @var OwnerService */
    private $service;

    public function __construct(OwnerService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *     tags={"Owners"},
     *     path="/api/owners",
     *     summary="Get list of owners",
     *     description="Get list of owners",
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort criterion: any entity field",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         description="Order criterion: asc or desc",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="List of clients"
     *     )
     * )
     *
     * @Rest\Get("/owners")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        return $this->view($this->service->getAll(
            $request->get('sort', 'id'),
            $request->get('order', 'ASC')
        ), Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *     tags={"Owners"},
     *     path="/api/owners",
     *     summary="Create new owner",
     *     description="Create new owner",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=OwnerStoreRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Owner object"
     *     )
     * )
     *
     * @Rest\Post("/owners")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param OwnerStoreRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     */
    public function store(OwnerStoreRequest $request, ValidatorInterface $validator)
    {
        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->store($request), Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     tags={"Owners"},
     *     path="/api/owners/{ownerId}",
     *     summary="Get existing owner",
     *     description="Get existing owner",
     *     @SWG\Response(
     *         response=200,
     *         description="Owner object"
     *     )
     * )
     *
     * @Rest\Get("/owners/{ownerId}")
     *
     * @param int $ownerId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function show(int $ownerId)
    {
        if (!($owner = $this->service->getById($ownerId))) {
            throw new EntityNotFoundException("Owner with id {$ownerId} does not exists.");
        }

        return $this->view($owner, Response::HTTP_OK);
    }

    /**
     * @SWG\Put(
     *     tags={"Owners"},
     *     path="/api/owners/{ownerId}",
     *     summary="Update existing owner",
     *     description="Update existing owner",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=OwnerUpdateRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Owner object"
     *     )
     * )
     *
     * @Rest\Put("/owners/{ownerId}")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param int $ownerId
     * @param OwnerUpdateRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function update(int $ownerId, OwnerUpdateRequest $request, ValidatorInterface $validator)
    {
        if (!($owner = $this->service->getById($ownerId))) {
            throw new EntityNotFoundException("Owner with id {$ownerId} does not exists.");
        }

        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->update($owner, $request), Response::HTTP_OK);
    }

    /**
     * @SWG\Delete(
     *     tags={"Owners"},
     *     path="/api/owners/{ownerId}",
     *     summary="Delete existing owner",
     *     description="Delete existing owner",
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     )
     * )
     *
     * @Rest\Delete("/owners/{ownerId}")
     *
     * @param int $ownerId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function destroy(int $ownerId)
    {
        if (!($owner = $this->service->getById($ownerId))) {
            throw new EntityNotFoundException("Owner with id {$ownerId} does not exists.");
        }

        return $this->view($this->service->destroy($owner), Response::HTTP_NO_CONTENT);
    }
}
