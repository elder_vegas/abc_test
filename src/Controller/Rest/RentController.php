<?php

namespace App\Controller\Rest;

use App\Dto\Request\Rent\RentStoreRequest;
use App\Dto\Request\Rent\RentUpdateRequest;
use App\Service\RentService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class RentController extends AbstractFOSRestController
{
    /** @var RentService */
    private $service;

    public function __construct(RentService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *     tags={"Rent"},
     *     path="/api/rents",
     *     summary="Get list of rent",
     *     description="Get list of rent",
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort criterion: any entity field",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         description="Order criterion: asc or desc",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="List of rent"
     *     )
     * )
     *
     * @Rest\Get("/rents")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        return $this->view($this->service->getAll(
            $request->get('sort', 'id'),
            $request->get('order', 'ASC')
        ), Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *     tags={"Rent"},
     *     path="/api/rents",
     *     summary="Create new rent",
     *     description="Create new rent",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=RentStoreRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Rent object"
     *     )
     * )
     *
     * @Rest\Post("/rents")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param RentStoreRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     */
    public function store(RentStoreRequest $request, ValidatorInterface $validator)
    {
        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->store($request), Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     tags={"Rent"},
     *     path="/api/rents/{rentId}",
     *     summary="Get existing rent",
     *     description="Get existing rent",
     *     @SWG\Response(
     *         response=200,
     *         description="Rent object"
     *     )
     * )
     *
     * @Rest\Get("/rents/{rentId}")
     *
     * @param int $rentId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function show(int $rentId)
    {
        if (!($rent = $this->service->getById($rentId))) {
            throw new EntityNotFoundException("Rent with id {$rentId} does not exists.");
        }

        return $this->view($rent, Response::HTTP_OK);
    }

    /**
     * @SWG\Put(
     *     tags={"Rent"},
     *     path="/api/rents/{rentId}",
     *     summary="Update existing rent",
     *     description="Update existing rent",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=RentUpdateRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Rent object"
     *     )
     * )
     *
     * @Rest\Put("/rents/{rentId}")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param int $rentId
     * @param RentUpdateRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function update(int $rentId, RentUpdateRequest $request, ValidatorInterface $validator)
    {
        if (!($rent = $this->service->getById($rentId))) {
            throw new EntityNotFoundException("Rent with id {$rentId} does not exists.");
        }

        if (new \DateTime() >= $rent->getDateStart()) {
            return $this->view(['message' => 'Rent has already begun.'], Response::HTTP_FORBIDDEN);
        }

        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->update($rent, $request), Response::HTTP_OK);
    }

    /**
     * @SWG\Delete(
     *     tags={"Rent"},
     *     path="/api/rents/{rentId}",
     *     summary="Delete existing rent",
     *     description="Delete existing rent",
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     )
     * )
     *
     * @Rest\Delete("/rents/{rentId}")
     *
     * @param int $rentId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function destroy(int $rentId)
    {
        if (!($rent = $this->service->getById($rentId))) {
            throw new EntityNotFoundException("Rent with id {$rentId} does not exists.");
        }

        if (new \DateTime() >= $rent->getDateStart()) {
            return $this->view(['message' => 'Rent has already begun.'], Response::HTTP_FORBIDDEN);
        }

        return $this->view($this->service->destroy($rent), Response::HTTP_NO_CONTENT);
    }
}
