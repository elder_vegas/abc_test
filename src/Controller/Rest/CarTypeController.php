<?php

namespace App\Controller\Rest;

use App\Repository\CarTypeRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

class CarTypeController extends AbstractFOSRestController
{
    /** @var CarTypeRepository */
    private $repository;

    public function __construct(CarTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @SWG\Get(
     *     tags={"Car Types"},
     *     path="/api/car/types",
     *     summary="Get list of car types",
     *     description="Get list of car types",
     *     @SWG\Response(
     *         response=200,
     *         description="List of car types"
     *     )
     * )
     *
     * @Rest\Get("/car/types", name="car_type")
     */
    public function index()
    {
        return $this->view($this->repository->findAll(), Response::HTTP_OK);
    }
}
