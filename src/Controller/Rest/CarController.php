<?php

namespace App\Controller\Rest;

use App\Dto\Request\Car\CarStoreRequest;
use App\Dto\Request\Car\CarUpdateRequest;
use App\Service\CarService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class CarController extends AbstractFOSRestController
{
    /** @var CarService */
    private $service;

    public function __construct(CarService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *     tags={"Cars"},
     *     path="/api/cars",
     *     summary="Get list of cars",
     *     description="Get list of cars",
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort criterion: any entity field",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         description="Order criterion: asc or desc",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="List of cars"
     *     )
     * )
     *
     * @Rest\Get("/cars")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        return $this->view($this->service->getAll(
            $request->get('sort', 'id'),
            $request->get('order', 'ASC')
        ), Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *     tags={"Cars"},
     *     path="/api/cars",
     *     summary="Create new car",
     *     description="Create new car",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=CarStoreRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Car object"
     *     )
     * )
     *
     * @Rest\Post("/cars")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param CarStoreRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     */
    public function store(CarStoreRequest $request, ValidatorInterface $validator)
    {
        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->store($request), Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     tags={"Cars"},
     *     path="/api/cars/{carId}",
     *     summary="Get existing car",
     *     description="Get existing car",
     *     @SWG\Response(
     *         response=200,
     *         description="Car object"
     *     )
     * )
     *
     * @Rest\Get("/cars/{carId}")
     *
     * @param int $carId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function show(int $carId)
    {
        if (!($car = $this->service->getById($carId))) {
            throw new EntityNotFoundException("Car with id {$carId} does not exists.");
        }

        return $this->view($car, Response::HTTP_OK);
    }

    /**
     * @SWG\Put(
     *     tags={"Cars"},
     *     path="/api/cars/{carId}",
     *     summary="Update existing car",
     *     description="Update existing car",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=CarUpdateRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Car object"
     *     )
     * )
     *
     * @Rest\Put("/cars/{carId}")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param int $carId
     * @param CarUpdateRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function update(int $carId, CarUpdateRequest $request, ValidatorInterface $validator)
    {
        if (!($car = $this->service->getById($carId))) {
            throw new EntityNotFoundException("Car with id {$carId} does not exists.");
        }

        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->update($car, $request), Response::HTTP_OK);
    }

    /**
     * @SWG\Delete(
     *     tags={"Cars"},
     *     path="/api/cars/{carId}",
     *     summary="Delete existing car",
     *     description="Delete existing car",
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     )
     * )
     *
     * @Rest\Delete("/cars/{carId}")
     *
     * @param int $carId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function destroy(int $carId)
    {
        if (!($car = $this->service->getById($carId))) {
            throw new EntityNotFoundException("Car with id {$carId} does not exists.");
        }

        return $this->view($this->service->destroy($car), Response::HTTP_NO_CONTENT);
    }
}
