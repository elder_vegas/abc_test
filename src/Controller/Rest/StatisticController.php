<?php

namespace App\Controller\Rest;

use App\Dto\Request\Statistic\StatisticByDay;
use App\Service\RentService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class StatisticController extends AbstractFOSRestController
{
    /** @var RentService */
    private $service;

    public function __construct(RentService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *     tags={"Statistics"},
     *     path="/api/statistics/cars",
     *     summary="Get statistic by popular cars",
     *     description="Get statistic by popular cars",
     *     @SWG\Response(
     *         response=200,
     *         description="Statistic info"
     *     )
     * )
     *
     * @Rest\Get("/statistics/cars")
     *
     * @return \FOS\RestBundle\View\View
     */
    public function byPopularCar()
    {
        return $this->view($this->service->statisticByPopularCar(), Response::HTTP_CREATED);
    }

    /**
     * @SWG\Post(
     *     tags={"Statistics"},
     *     path="/api/statistics/days",
     *     summary="Get statistic by days",
     *     description="Get statistic by days",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=StatisticByDay::class))
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Statistic info"
     *     )
     * )
     *
     * @Rest\Post("/statistics/days")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param StatisticByDay $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     */
    public function byDays(StatisticByDay $request, ValidatorInterface $validator)
    {
        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view(
            $this->service->statisticByDay($request->getDateFrom(), $request->getDateTo()),
            Response::HTTP_CREATED
        );
    }
}