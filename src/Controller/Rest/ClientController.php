<?php

namespace App\Controller\Rest;

use App\Dto\Request\Client\ClientStoreRequest;
use App\Dto\Request\Client\ClientUpdateRequest;
use App\Service\ClientService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class ClientController extends AbstractFOSRestController
{
    /** @var ClientService */
    private $service;

    public function __construct(ClientService $service)
    {
        $this->service = $service;
    }

    /**
     * @SWG\Get(
     *     tags={"Clients"},
     *     path="/api/clients",
     *     summary="Get list of clients",
     *     description="Get list of clients",
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort criterion: any entity field",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="order",
     *         in="query",
     *         description="Order criterion: asc or desc",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="List of clients"
     *     )
     * )
     *
     * @Rest\Get("/clients")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        return $this->view($this->service->getAll(
            $request->get('sort', 'id'),
            $request->get('order', 'ASC')
        ), Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *     tags={"Clients"},
     *     path="/api/clients",
     *     summary="Create new client",
     *     description="Create new client",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=ClientStoreRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Client object"
     *     )
     * )
     *
     * @Rest\Post("/clients")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param ClientStoreRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     */
    public function store(ClientStoreRequest $request, ValidatorInterface $validator)
    {
        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->store($request), Response::HTTP_CREATED);
    }

    /**
     * @SWG\Get(
     *     tags={"Clients"},
     *     path="/api/clients/{clientId}",
     *     summary="Get existing client",
     *     description="Get existing client",
     *     @SWG\Response(
     *         response=200,
     *         description="Client object"
     *     )
     * )
     *
     * @Rest\Get("/clients/{clientId}")
     *
     * @param int $clientId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function show(int $clientId)
    {
        if (!($client = $this->service->getById($clientId))) {
            throw new EntityNotFoundException("Client with id {$clientId} does not exists.");
        }

        return $this->view($client, Response::HTTP_OK);
    }

    /**
     * @SWG\Put(
     *     tags={"Clients"},
     *     path="/api/clients/{clientId}",
     *     summary="Update existing client",
     *     description="Update existing client",
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref=@Model(type=ClientUpdateRequest::class))
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Client object"
     *     )
     * )
     *
     * @Rest\Put("/clients/{clientId}")
     * @ParamConverter("request", converter="fos_rest.request_body")
     *
     * @param int $clientId
     * @param ClientUpdateRequest $request
     * @param ValidatorInterface $validator
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function update(int $clientId, ClientUpdateRequest $request, ValidatorInterface $validator)
    {
        if (!($client = $this->service->getById($clientId))) {
            throw new EntityNotFoundException("Client with id {$clientId} does not exists.");
        }

        $errors = $validator->validate($request);
        if (count($errors) > 0) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->view($this->service->update($client, $request), Response::HTTP_OK);
    }

    /**
     * @SWG\Delete(
     *     tags={"Clients"},
     *     path="/api/clients/{clientId}",
     *     summary="Delete existing client",
     *     description="Delete existing client",
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     )
     * )
     *
     * @Rest\Delete("/clients/{clientId}")
     *
     * @param int $clientId
     * @return \FOS\RestBundle\View\View
     * @throws EntityNotFoundException
     */
    public function destroy(int $clientId)
    {
        if (!($client = $this->service->getById($clientId))) {
            throw new EntityNotFoundException("Client with id {$clientId} does not exists.");
        }

        return $this->view($this->service->destroy($client), Response::HTTP_NO_CONTENT);
    }
}
