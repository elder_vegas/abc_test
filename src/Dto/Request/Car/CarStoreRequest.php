<?php

namespace App\Dto\Request\Car;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

final class CarStoreRequest
{
    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $type_id;

    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $owner_id;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $fuel;

    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $capacity;

    /**
     * @Assert\Type("bool")
     * @Assert\NotNull()
     * @Type("boolean")
     */
    private $active;

    /**
     * @Type("string")
     */
    private $comment;

    /**
     * CarStoreRequest constructor.
     * @param $type_id
     * @param $owner_id
     * @param $name
     * @param $fuel
     * @param $capacity
     * @param $active
     * @param $comment
     */
    public function __construct($type_id, $owner_id, $name, $fuel, $capacity, $active, $comment)
    {
        $this->type_id = $type_id;
        $this->owner_id = $owner_id;
        $this->name = $name;
        $this->fuel = $fuel;
        $this->capacity = $capacity;
        $this->active = $active;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @return mixed
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}
