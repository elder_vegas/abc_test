<?php

namespace App\Dto\Request\Car;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

final class CarUpdateRequest
{
    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $fuel;

    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $capacity;

    /**
     * @Assert\Type("bool")
     * @Assert\NotNull()
     * @Type("boolean")
     */
    private $active;

    /**
     * @Type("string")
     */
    private $comment;

    /**
     * CarUpdateRequest constructor.
     * @param $name
     * @param $fuel
     * @param $capacity
     * @param $active
     * @param $comment
     */
    public function __construct($name, $fuel, $capacity, $active, $comment)
    {
        $this->name = $name;
        $this->fuel = $fuel;
        $this->capacity = $capacity;
        $this->active = $active;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @return mixed
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}
