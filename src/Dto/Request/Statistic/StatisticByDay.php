<?php

namespace App\Dto\Request\Statistic;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

class StatisticByDay
{
    /**
     * @Assert\Date()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_from;

    /**
     * @Assert\Date()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_to;

    /**
     * StatisticByDay constructor.
     * @param $date_from
     * @param $date_to
     */
    public function __construct($date_from, $date_to)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }

    /**
     * @return mixed
     */
    public function getDateFrom()
    {
        return $this->date_from;
    }

    /**
     * @return mixed
     */
    public function getDateTo()
    {
        return $this->date_to;
    }
}
