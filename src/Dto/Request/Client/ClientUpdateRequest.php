<?php

namespace App\Dto\Request\Client;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

final class ClientUpdateRequest
{
    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $fio;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $passport;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $license;

    /**
     * @Assert\NotBlank()
     * @Type("string")
     */
    private $phone;

    /**
     * OwnerStoreRequest constructor.
     * @param $fio
     * @param $passport
     * @param $license
     * @param $phone
     */
    public function __construct(string $fio, string $passport, string $license, string $phone)
    {
        $this->fio = $fio;
        $this->passport = $passport;
        $this->license = $license;
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @return mixed
     */
    public function getPassport(): string
    {
        return $this->passport;
    }

    /**
     * @return mixed
     */
    public function getLicense(): string
    {
        return $this->license;
    }

    /**
     * @return mixed
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}
