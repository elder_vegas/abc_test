<?php

namespace App\Dto\Request\Rent;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

final class RentStoreRequest
{
    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $client_id;

    /**
     * @Assert\Type("int")
     * @Assert\Positive()
     * @Assert\NotNull()
     * @Type("int")
     */
    private $car_id;

    /**
     * @Assert\DateTime()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_start;

    /**
     * @Assert\DateTime()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_end;

    /**
     * @Type("string")
     */
    private $comment;

    /**
     * RentStoreRequest constructor.
     * @param $client_id
     * @param $car_id
     * @param $date_start
     * @param $date_end
     * @param $comment
     */
    public function __construct($client_id, $car_id, $date_start, $date_end, $comment)
    {
        $this->client_id = $client_id;
        $this->car_id = $car_id;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @return mixed
     */
    public function getCarId()
    {
        return $this->car_id;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}
