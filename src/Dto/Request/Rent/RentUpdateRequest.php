<?php

namespace App\Dto\Request\Rent;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

final class RentUpdateRequest
{
    /**
     * @Assert\DateTime()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_start;

    /**
     * @Assert\DateTime()
     * @Assert\NotNull()
     * @Type("string")
     */
    private $date_end;

    /**
     * @Type("string")
     */
    private $comment;

    /**
     * RentUpdateRequest constructor.
     * @param $date_start
     * @param $date_end
     * @param $comment
     */
    public function __construct($date_start, $date_end, $comment)
    {
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}
