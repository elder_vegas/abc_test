<?php

namespace App\Repository;

use App\Entity\Rent;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Validator\Constraints\Date;

class RentRepository extends CommonRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rent::class);
    }

    public function statisticByDay($dateFrom, $dateTo)
    {
        $now = (new \DateTime())->format('Y-m-d');

        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare("
            select 
              date (date_end) as date, 
              ROUND(SUM(price * 0.25)) as sum_revenue
            from rent
            where date_end between :date_from and :date_to
              and date_end < :now
            group by date
        ");
        $stmt->execute([
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'now' => $now
        ]);

        return $stmt->fetchAll();
    }

    public function statisticByPopularCar()
    {
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare("
            select
              c.id,
              c.name,
              COUNT(*) as rent_amount,
              SUM(TIMESTAMPDIFF(HOUR, r.date_start, r.date_end)) as rent_time,
              ROUND(SUM(price * 0.25)) as sum_revenue
            from car c
            join rent r on c.id = r.car_id
            group by c.id
            having sum_revenue > 1000
            order by rent_time desc
        ");
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
