<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Common\Persistence\ManagerRegistry;

class CarRepository extends CommonRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }
}
