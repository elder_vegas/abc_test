<?php

namespace App\Repository;

use App\Entity\Owner;
use Doctrine\Common\Persistence\ManagerRegistry;

class OwnerRepository extends CommonRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Owner::class);
    }
}
