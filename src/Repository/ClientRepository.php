<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Common\Persistence\ManagerRegistry;

class ClientRepository extends CommonRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }
}
