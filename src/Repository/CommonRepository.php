<?php

namespace App\Repository;

use App\Entity\IEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method IEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method IEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method IEntity[]    findAll()
 * @method IEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class CommonRepository extends ServiceEntityRepository
{
    public function save(IEntity $entity)
    {
        $entity->setCreatedAt(new \DateTime());
        $entity->setUpdatedAt(new \DateTime());
        $this->_em->persist($entity);
        $this->_em->flush();

        return $entity;
    }

    public function update(IEntity $entity)
    {
        $entity->setUpdatedAt(new \DateTime());
        $this->_em->flush();

        return $entity;
    }

    public function delete(IEntity $entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();

        return $entity;
    }

//    /**
//     * @return IEntity[] Returns an array of IEntity objects
//     */
//    public function findByExampleField($value)
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    /**
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function findAllWithSort($sort = 'id', $order = 'ASC')
    {
        return $this->createQueryBuilder('o')
            ->orderBy("o.{$sort}", $order)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?IEntity
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
