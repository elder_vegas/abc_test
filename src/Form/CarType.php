<?php

namespace App\Form;

use App\Entity\Car;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type_id', TextType::class, [
                'required' => true,
                'constraints' => [new Type('int')]
            ])
            ->add('owner_id', TextType::class, [
                'required' => true,
                'constraints' => [new Type('int')]
            ])
            ->add('name')
            ->add('fuel')
            ->add('capacity')
            ->add('active')
            ->add('comment', TextType::class, [
                'required' => false,
                'constraints' => [new NotNull()]
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Car::class,
            'csrf_protection' => false
        ));
    }
}
