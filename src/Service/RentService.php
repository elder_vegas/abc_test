<?php

namespace App\Service;

use App\Dto\Request\Rent\RentStoreRequest;
use App\Dto\Request\Rent\RentUpdateRequest;
use App\Entity\Rent;
use App\Repository\RentRepository;
use App\Service\Converter\RentConverter;

class RentService
{
    /** @var RentRepository */
    private $repository;

    /** @var RentConverter */
    private $converter;

    /**
     * RentService constructor.
     * @param RentRepository $repository
     * @param RentConverter $converter
     */
    public function __construct(RentRepository $repository, RentConverter $converter)
    {
        $this->repository = $repository;
        $this->converter = $converter;
    }

    public function getAll(string $sort, string $order)
    {
        return $this->repository->findAllWithSort($sort, $order);
    }

    public function store(RentStoreRequest $request)
    {
        $owner = $this->converter->storeRequestToRent($request);

        return $this->repository->save($owner);
    }

    public function getById(int $ownerId)
    {
        return $this->repository->find($ownerId);
    }

    public function update(Rent $rent, RentUpdateRequest $request)
    {
        return $this->repository->update($this->converter->updateRequestToRent($request, $rent));
    }

    public function destroy(Rent $rent)
    {
        return $this->repository->delete($rent);
    }

    public function statisticByDay($dateFrom, $dateTo)
    {
        return $this->repository->statisticByDay($dateFrom, $dateTo);
    }

    public function statisticByPopularCar()
    {
        return $this->repository->statisticByPopularCar();
    }
}
