<?php

namespace App\Service\Converter;

use App\Dto\Request\Car\CarStoreRequest;
use App\Dto\Request\Car\CarUpdateRequest;
use App\Entity\Car;
use App\Repository\CarTypeRepository;
use App\Repository\OwnerRepository;
use Doctrine\ORM\EntityNotFoundException;

class CarConverter
{
    /** @var OwnerRepository */
    private $ownerRepository;

    /** @var CarTypeRepository */
    private $carTypeRepository;

    /**
     * CarConverter constructor.
     * @param OwnerRepository $ownerRepository
     * @param CarTypeRepository $carTypeRepository
     */
    public function __construct(OwnerRepository $ownerRepository, CarTypeRepository $carTypeRepository)
    {
        $this->ownerRepository = $ownerRepository;
        $this->carTypeRepository = $carTypeRepository;
    }

    /**
     * @param CarStoreRequest $request
     * @return Car
     * @throws EntityNotFoundException
     */
    public function storeRequestToCar(CarStoreRequest $request)
    {
        $type = $this->carTypeRepository->find($request->getTypeId());
        if (!$type) {
            throw new EntityNotFoundException("Type with id {$request->getTypeId()} does not exists.");
        }
        $owner = $this->ownerRepository->find($request->getOwnerId());
        if (!$owner) {
            throw new EntityNotFoundException("Owner with id {$request->getOwnerId()} does not exists.");
        }

        $car = new Car();
        $car->setName($request->getName());
        $car->setFuel($request->getFuel());
        $car->setCapacity($request->getCapacity());
        $car->setActive($request->getActive());
        $car->setComment($request->getComment());
        $car->setType($type);
        $car->setOwner($owner);
        $car->setCreatedAt(new \DateTime());
        $car->setUpdatedAt(new \DateTime());

        return $car;
    }

    /**
     * @param CarUpdateRequest $request
     * @param Car $car
     * @return Car
     * @throws \Exception
     */
    public function updateRequestToCar(CarUpdateRequest $request, Car $car)
    {
        $car->setName($request->getName());
        $car->setFuel($request->getFuel());
        $car->setCapacity($request->getCapacity());
        $car->setActive($request->getActive());
        $car->setComment($request->getComment());
        $car->setUpdatedAt(new \DateTime());

        return $car;
    }
}
