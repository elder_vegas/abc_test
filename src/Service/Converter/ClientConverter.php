<?php

namespace App\Service\Converter;

use App\Dto\Request\Client\ClientStoreRequest;
use App\Dto\Request\Client\ClientUpdateRequest;
use App\Entity\Client;

class ClientConverter
{
    /**
     * @param ClientStoreRequest $request
     * @return Client
     * @throws \Exception
     */
    public function storeRequestToClient(ClientStoreRequest $request)
    {
        $client = new Client();
        $client->setFio($request->getFio());
        $client->setPassport($request->getPassport());
        $client->setLicense($request->getLicense());
        $client->setPhone($request->getPhone());
        $client->setCreatedAt(new \DateTime());
        $client->setUpdatedAt(new \DateTime());

        return $client;
    }

    /**
     * @param ClientUpdateRequest $request
     * @param Client $client
     * @return Client
     * @throws \Exception
     */
    public function updateRequestToClient(ClientUpdateRequest $request, Client $client)
    {
        $client->setFio($request->getFio());
        $client->setPassport($request->getPassport());
        $client->setLicense($request->getLicense());
        $client->setPhone($request->getPhone());
        $client->setUpdatedAt(new \DateTime());

        return $client;
    }
}
