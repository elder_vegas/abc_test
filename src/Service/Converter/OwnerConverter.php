<?php

namespace App\Service\Converter;

use App\Dto\Request\Owner\OwnerStoreRequest;
use App\Dto\Request\Owner\OwnerUpdateRequest;
use App\Entity\Owner;

class OwnerConverter
{
    /**
     * @param OwnerStoreRequest $request
     * @return Owner
     * @throws \Exception
     */
    public function storeRequestToOwner(OwnerStoreRequest $request)
    {
        $owner = new Owner();
        $owner->setFio($request->getFio());
        $owner->setPassport($request->getPassport());
        $owner->setLicense($request->getLicense());
        $owner->setPhone($request->getPhone());
        $owner->setCreatedAt(new \DateTime());
        $owner->setUpdatedAt(new \DateTime());

        return $owner;
    }

    /**
     * @param OwnerUpdateRequest $request
     * @param Owner $owner
     * @return Owner
     * @throws \Exception
     */
    public function updateRequestToOwner(OwnerUpdateRequest $request, Owner $owner)
    {
        $owner->setFio($request->getFio());
        $owner->setPassport($request->getPassport());
        $owner->setLicense($request->getLicense());
        $owner->setPhone($request->getPhone());
        $owner->setUpdatedAt(new \DateTime());

        return $owner;
    }
}
