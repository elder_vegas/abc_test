<?php

namespace App\Service\Converter;

use App\Dto\Request\Rent\RentStoreRequest;
use App\Dto\Request\Rent\RentUpdateRequest;
use App\Entity\Rent;
use App\Repository\CarRepository;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityNotFoundException;

class RentConverter
{
    /** @var CarRepository */
    private $carRepository;

    /** @var ClientRepository */
    private $clientRepository;

    /**
     * RentConverter constructor.
     * @param CarRepository $carRepository
     * @param ClientRepository $clientRepository
     */
    public function __construct(CarRepository $carRepository, ClientRepository $clientRepository)
    {
        $this->carRepository = $carRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param RentStoreRequest $request
     * @return Rent
     * @throws EntityNotFoundException
     */
    public function storeRequestToRent(RentStoreRequest $request)
    {
        $car = $this->carRepository->find($request->getCarId());
        if (!$car) {
            throw new EntityNotFoundException("Car with id {$request->getCarId()} does not exists.");
        }
        $client = $this->clientRepository->find($request->getClientId());
        if (!$client) {
            throw new EntityNotFoundException("Client with id {$request->getClientId()} does not exists.");
        }

        $rent = new Rent();
        $rent->setCar($car);
        $rent->setClient($client);
        $rent->setDateStart(new \DateTime($request->getDateStart()));
        $rent->setDateEnd(new \DateTime($request->getDateEnd()));
        $rent->setPrice($this->calcPrice($request->getDateStart(), $request->getDateEnd()));
        $rent->setComment($request->getComment());
        $rent->setCreatedAt(new \DateTime());
        $rent->setUpdatedAt(new \DateTime());

        return $rent;
    }

    /**
     * @param RentUpdateRequest $request
     * @param Rent $rent
     * @return Rent
     * @throws \Exception
     */
    public function updateRequestToRent(RentUpdateRequest $request, Rent $rent)
    {
        $rent->setDateStart(new \DateTime($request->getDateStart()));
        $rent->setDateEnd(new \DateTime($request->getDateEnd()));
        $rent->setPrice($this->calcPrice($request->getDateStart(), $request->getDateEnd()));
        $rent->setComment($request->getComment());
        $rent->setUpdatedAt(new \DateTime());

        return $rent;
    }

    private function calcPrice(string $dateStart, string $dateEnd)
    {
        $d1 = new \DateTime($dateStart);
        $d2 = new \DateTime($dateEnd);
        $diff = $d1->diff($d2);

        return ($diff->d === 0) ? $diff->h * 200 : ($diff->d * 2000) + ($diff->h * 100);
    }
}
