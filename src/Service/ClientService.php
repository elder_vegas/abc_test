<?php

namespace App\Service;

use App\Dto\Request\Client\ClientStoreRequest;
use App\Dto\Request\Client\ClientUpdateRequest;
use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Service\Converter\ClientConverter;

class ClientService
{
    /** @var ClientRepository */
    private $repository;

    /** @var ClientConverter */
    private $converter;

    /**
     * ClientService constructor.
     * @param ClientRepository $repository
     * @param ClientConverter $converter
     */
    public function __construct(ClientRepository $repository, ClientConverter $converter)
    {
        $this->repository = $repository;
        $this->converter = $converter;
    }

    public function getAll(string $sort, string $order)
    {
        return $this->repository->findAllWithSort($sort, $order);
    }

    public function store(ClientStoreRequest $request)
    {
        $owner = $this->converter->storeRequestToClient($request);

        return $this->repository->save($owner);
    }

    public function getById(int $ownerId)
    {
        return $this->repository->find($ownerId);
    }

    public function update(Client $client, ClientUpdateRequest $request)
    {
        return $this->repository->update($this->converter->updateRequestToClient($request, $client));
    }

    public function destroy(Client $client)
    {
        return $this->repository->delete($client);
    }
}
