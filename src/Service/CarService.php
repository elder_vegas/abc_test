<?php

namespace App\Service;

use App\Dto\Request\Car\CarStoreRequest;
use App\Dto\Request\Car\CarUpdateRequest;
use App\Entity\Car;
use App\Repository\CarRepository;
use App\Service\Converter\CarConverter;

class CarService
{
    /** @var CarRepository */
    private $repository;

    /** @var CarConverter */
    private $converter;

    /**
     * CarService constructor.
     * @param CarRepository $repository
     * @param CarConverter $converter
     */
    public function __construct(CarRepository $repository, CarConverter $converter)
    {
        $this->repository = $repository;
        $this->converter = $converter;
    }

    public function getAll(string $sort, string $order)
    {
        return $this->repository->findAllWithSort($sort, $order);
    }

    public function store(CarStoreRequest $request)
    {
        $owner = $this->converter->storeRequestToCar($request);

        return $this->repository->save($owner);
    }

    public function getById(int $ownerId)
    {
        return $this->repository->find($ownerId);
    }

    public function update(Car $car, CarUpdateRequest $request)
    {
        return $this->repository->update($this->converter->updateRequestToCar($request, $car));
    }

    public function destroy(Car $car)
    {
        return $this->repository->delete($car);
    }
}
