<?php

namespace App\Service;

use App\Dto\Request\Owner\OwnerStoreRequest;
use App\Dto\Request\Owner\OwnerUpdateRequest;
use App\Entity\Owner;
use App\Repository\OwnerRepository;
use App\Service\Converter\OwnerConverter;

class OwnerService
{
    /** @var OwnerRepository */
    private $repository;

    /** @var OwnerConverter */
    private $converter;

    /**
     * OwnerService constructor.
     * @param OwnerRepository $repository
     * @param OwnerConverter $converter
     */
    public function __construct(OwnerRepository $repository, OwnerConverter $converter)
    {
        $this->repository = $repository;
        $this->converter = $converter;
    }

    public function getAll(string $sort, string $order)
    {
        return $this->repository->findAllWithSort($sort, $order);
    }

    public function store(OwnerStoreRequest $request)
    {
        $owner = $this->converter->storeRequestToOwner($request);

        return $this->repository->save($owner);
    }

    public function getById(int $ownerId)
    {
        return $this->repository->find($ownerId);
    }

    public function update(Owner $owner, OwnerUpdateRequest $request)
    {
        return $this->repository->update($this->converter->updateRequestToOwner($request, $owner));
    }

    public function destroy(Owner $owner)
    {
        return $this->repository->delete($owner);
    }
}
