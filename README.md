# АБК - Тестовое задание

###### Стек
- Symfony 4.3
- Mysql 14.14
- Пакет для работы с jwt: lexik/jwt-authentication-bundle
- Пакет для генерации документации: nelmio/api-doc-bundle

###### Установка
- composer install
- внести изменения в env:
    * DATABASE_URL: добавить имя пользователя и пароль, имя базы данных
    * JWT_PASSPHRASE: установить секретное слово, которое будет использовано для генерации ключей jwt. Можно не изменять.
- генерация ключей jwt:
    * mkdir -p config/jwt
    * openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    * openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
- bin/console doctrine:migrations:migrate
- накатить дамп базы
- bin/console server:start
- Документация по API: http://127.0.0.1:8000/api/doc